# Order Demo Application using Axon4 framework with Kafka

###### Order --> Shipment example using Axon Saga
* Saga starts when PlaceOrderCmd is executed
* Shipment is created and gets registered against Order
* Once ShipmentArrivedCmd is executed, end saga is executed


###### Steps to build and deploy

- Go to folder infra and run docker-compose to create docker instance of zookeepr, kafka, Prostgres command & query database  

```
docker-compose -f docker-compose.yml build
docker-compose -f docker-compose.yml up -d
```

- Set docker IP or host IP address as environment variable

```
set AXON_INFRA_HOST_IP=...
```
- Build application

```
mvn clean install
```

- Go to cmd-order folder and run command application  

```
mvn spring-boot:run
```

- Go to cmd-shipment folder and run command application  

```
mvn spring-boot:run
```

- Go to query folder and run query application 

```
mvn spring-boot:run
```

- Once order command application it deployed, swagger is available:

	 
	http://localhost:9001/
	
- Once shipment command application it deployed, swagger is available:

	 
	http://localhost:9002/

- Once query application is deployed, swagger is available:

	http://localhost:9003/