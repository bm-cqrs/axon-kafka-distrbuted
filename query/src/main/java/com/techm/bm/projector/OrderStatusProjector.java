package com.techm.bm.projector;

import javax.persistence.EntityManager;

import org.axonframework.config.ProcessingGroup;
import org.axonframework.eventhandling.EventHandler;
import org.axonframework.queryhandling.QueryHandler;
import org.springframework.stereotype.Component;

import com.techm.bm.api.evt.OrderPlacedEvt;
import com.techm.bm.api.evt.OrderClosedEvt;
import com.techm.bm.api.evt.OrderShipmentSentEvt;
import com.techm.bm.view.model.OrderStatus;
import com.techm.bm.view.query.FindOrderStatus;

import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
@ProcessingGroup("query-processor")
public class OrderStatusProjector {

    private final EntityManager entityManager;

    @EventHandler
    public void on(OrderPlacedEvt evt) {
        entityManager.persist(OrderStatus.builder()
                .orderId(evt.getOrderId())
                .shipmentId(null)
                .delivered(false)
                .build());
    }

    @EventHandler
    public void on(OrderShipmentSentEvt evt) {
        OrderStatus status = entityManager.find(OrderStatus.class, evt.getOrderId());
        status.setShipmentId(evt.getShipmentId());
    }

    @EventHandler
    public void on(OrderClosedEvt evt) {
        OrderStatus status = entityManager.find(OrderStatus.class, evt.getOrderId());
        status.setDelivered(true);
    }

    @QueryHandler
    public OrderStatus handle(FindOrderStatus query) {
        return entityManager.find(OrderStatus.class, query.getOrderId());
    }

}
