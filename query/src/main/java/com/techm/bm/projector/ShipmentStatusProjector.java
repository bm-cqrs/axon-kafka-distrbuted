package com.techm.bm.projector;

import java.time.Instant;
import java.util.List;

import javax.persistence.EntityManager;

import org.axonframework.config.ProcessingGroup;
import org.axonframework.eventhandling.EventHandler;
import org.axonframework.eventhandling.Timestamp;
import org.axonframework.queryhandling.QueryHandler;
import org.springframework.stereotype.Component;

import com.techm.bm.api.evt.OrderShipmentDeliveredEvt;
import com.techm.bm.api.evt.ShipmentPreparedEvt;
import com.techm.bm.view.model.OpenShipment;
import com.techm.bm.view.query.ListAll;

import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
@ProcessingGroup("query-processor")
public class ShipmentStatusProjector {

	private final EntityManager entityManager;

	@EventHandler
	public void on(ShipmentPreparedEvt evt, @Timestamp Instant timestamp) {
		entityManager.persist(OpenShipment.builder().shipmentId(evt.getShipmentId()).destination(evt.getDestination())
				.registeredOn(timestamp).build());
	}

	@EventHandler
	public void on(OrderShipmentDeliveredEvt evt) {
		OpenShipment openShipment = entityManager.find(OpenShipment.class, evt.getShipmentId());
		entityManager.remove(openShipment);
	}

	@QueryHandler
	public List<OpenShipment> handle(ListAll query) {
		return entityManager.createQuery("SELECT e FROM OpenShipment e ORDER BY registeredOn ASC", OpenShipment.class)
				.getResultList();
	}

}
