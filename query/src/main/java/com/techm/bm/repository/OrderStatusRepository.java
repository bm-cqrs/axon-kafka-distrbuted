package com.techm.bm.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.techm.bm.view.model.OrderStatus;

@Repository
public interface OrderStatusRepository extends JpaRepository<OrderStatus, UUID> {
}