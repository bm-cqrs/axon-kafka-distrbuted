package com.techm.bm.api.evt;

import java.util.UUID;

import lombok.Value;

@Value
public class OrderPlacedEvt {

    UUID orderId;
    String goods;
    String destination;

}
