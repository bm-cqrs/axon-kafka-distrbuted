package com.techm.bm.api.cmd;

import lombok.Value;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

import java.util.UUID;

@Value
public class SendShipmentCmd implements Command {
    
    UUID orderId;

    @TargetAggregateIdentifier
    UUID shipmentId;

}
