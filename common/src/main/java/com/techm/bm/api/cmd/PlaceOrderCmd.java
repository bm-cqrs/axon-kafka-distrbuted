package com.techm.bm.api.cmd;

import java.util.UUID;

import org.axonframework.modelling.command.TargetAggregateIdentifier;

import lombok.Value;

@Value
public class PlaceOrderCmd implements Command {

    @TargetAggregateIdentifier
    UUID orderId;

    String goods;
    String destination;


}
