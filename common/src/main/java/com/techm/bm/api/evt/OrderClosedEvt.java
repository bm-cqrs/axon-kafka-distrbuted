package com.techm.bm.api.evt;

import lombok.Value;

import java.util.UUID;

@Value
public class OrderClosedEvt {

    UUID orderId;

    UUID shipmentId;

}
