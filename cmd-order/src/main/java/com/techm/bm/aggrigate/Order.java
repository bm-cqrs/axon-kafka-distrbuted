package com.techm.bm.aggrigate;

import static org.axonframework.modelling.command.AggregateLifecycle.apply;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.spring.stereotype.Aggregate;

import com.techm.bm.api.cmd.PlaceOrderCmd;
import com.techm.bm.api.evt.OrderPlacedEvt;
import com.techm.bm.api.evt.OrderClosedEvt;
import com.techm.bm.api.evt.OrderShipmentSentEvt;

import lombok.NoArgsConstructor;

@Aggregate
@NoArgsConstructor
public class Order {

	@AggregateIdentifier
	private UUID orderId;

	private Set<UUID> undeliveredShipments;

	@CommandHandler
	Order(PlaceOrderCmd cmd) {
		apply(new OrderPlacedEvt(cmd.getOrderId(), cmd.getGoods(), cmd.getDestination()));
	}

	@EventSourcingHandler
	void on(OrderPlacedEvt evt) {
		orderId = evt.getOrderId();
		undeliveredShipments = new HashSet<>();
	}

	@EventSourcingHandler
	void on(OrderShipmentSentEvt evt) {
		undeliveredShipments.add(evt.getShipmentId());
	}

	@EventSourcingHandler
	void on(OrderClosedEvt evt) {
		if (undeliveredShipments.contains(evt.getShipmentId())) {
			undeliveredShipments.remove(evt.getShipmentId());
		}
	}
}
