package com.techm.bm.aggrigate;

import static org.axonframework.modelling.command.AggregateLifecycle.apply;
import static org.axonframework.modelling.command.AggregateLifecycle.markDeleted;

import java.util.UUID;

import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.spring.stereotype.Aggregate;

import com.techm.bm.api.cmd.CloseOrderCmd;
import com.techm.bm.api.cmd.DeliverShipmentCmd;
import com.techm.bm.api.cmd.PrepareShipmentCmd;
import com.techm.bm.api.cmd.SendShipmentCmd;
import com.techm.bm.api.evt.OrderClosedEvt;
import com.techm.bm.api.evt.OrderShipmentDeliveredEvt;
import com.techm.bm.api.evt.ShipmentPreparedEvt;
import com.techm.bm.api.evt.OrderShipmentSentEvt;

import lombok.NoArgsConstructor;

@Aggregate
@NoArgsConstructor
class Shipment {

	@AggregateIdentifier
	private UUID shipmentId;

	@CommandHandler
	Shipment(PrepareShipmentCmd cmd) {
		apply(new ShipmentPreparedEvt(cmd.getShipmentId(), cmd.getDestination()));
	}

	@CommandHandler
	void handle(SendShipmentCmd cmd) {
		apply(new OrderShipmentSentEvt(cmd.getOrderId(), shipmentId));
	}

	@CommandHandler
	void handle(DeliverShipmentCmd cmd) {
		apply(new OrderShipmentDeliveredEvt(shipmentId));
	}
	
	@CommandHandler
	void handle(CloseOrderCmd cmd) {
		apply(new OrderClosedEvt(cmd.getOrderId(), cmd.getShipmentId()));
	}
	
	@EventSourcingHandler
	void on(ShipmentPreparedEvt evt) {
		shipmentId = evt.getShipmentId();
	}

	@EventSourcingHandler
	void on(OrderClosedEvt evt) {
		markDeleted();
	}

}
