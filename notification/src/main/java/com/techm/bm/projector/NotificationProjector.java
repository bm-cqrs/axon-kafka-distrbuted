package com.techm.bm.projector;

import org.axonframework.config.ProcessingGroup;
import org.axonframework.eventhandling.EventHandler;
import org.springframework.stereotype.Component;

import com.techm.bm.api.evt.OrderClosedEvt;
import com.techm.bm.api.evt.OrderPlacedEvt;
import com.techm.bm.api.evt.OrderShipmentDeliveredEvt;
import com.techm.bm.api.evt.OrderShipmentSentEvt;
import com.techm.bm.api.evt.ShipmentPreparedEvt;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Component
@RequiredArgsConstructor
@Slf4j
@ProcessingGroup("notification-processor")
public class NotificationProjector {

	@EventHandler
	public void on(OrderPlacedEvt evt) {
		log.debug("NOTIFICATION {}", evt);
		System.out.println("NOTIFICATION {}" + evt);
	}

	@EventHandler
	public void on(OrderShipmentSentEvt evt) {
		log.debug("NOTIFICATION {}", evt);
	}

	@EventHandler
	public void on(OrderClosedEvt evt) {
		log.debug("NOTIFICATION {}", evt);
	}

	@EventHandler
	public void on(ShipmentPreparedEvt evt) {
		log.debug("NOTIFICATION {}", evt);
	}

	@EventHandler
	public void on(OrderShipmentDeliveredEvt evt) {
		log.debug("NOTIFICATION {}", evt);
	}
}
